$(document).ready(function() {
    $(".slider").slick({
        speed: 2000,
        slidesToShow: 1,
        slidesToScroll: 1,
        //centerPadding: '24.2%',
        autoplay: true,
        autoplaySpeed: 3000,
        fade: false,
        easing: 'ease-out',
        //centerMode: true,
        dots: true,
        arrows: false,
        responsive: [{
            breakpoint: 980,
            settings: {
                arrows: false,
                centerMode: false,
                slidesToShow: 1
            }
        }]
    });
});