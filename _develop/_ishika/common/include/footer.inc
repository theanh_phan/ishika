<div class="top_ft">
    <div class="row">
        <h2 class="logo_ft"><a href="/">韋歯科医院<span>（い歯科）</span></a></h2>
        <p class="txt_tel">TEL:<a href="tel:06-6584-4182">06-6584-4182</a></p>
        <em class="txt_add">〒550-0027 大阪府大阪市西区九条2-5-4 <small>（無料駐車場2台あり）</small></em>
        <ul class="nav_ft">
            <li><a href="/">トップ</a></li>
            <li><a href="/clinical_info/">診療案内</a></li>
            <li><a href="/staff/">医院紹介</a></li>
            <li><a href="/access/">アクセス</a></li>
        </ul>
        <!--/.nav_ft-->
        <div class="b_flex">
            <div class="tbl">
                <table>
                    <thead>
                        <tr>
                            <th>診療時間</th>
                            <th>月</th>
                            <th>火</th>
                            <th>水</th>
                            <th>木</th>
                            <th>金</th>
                            <th>土</th>
                            <th>日</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>9：30～13：00</td>
                            <td>◯</td>
                            <td>◯</td>
                            <td>◯</td>
                            <td>◯</td>
                            <td>◯</td>
                            <td>△</td>
                            <td>ー</td>
                        </tr>
                        <tr>
                            <td>15：00～19：30</td>
                            <td>◯</td>
                            <td>◯</td>
                            <td>◯</td>
                            <td>◯</td>
                            <td>◯</td>
                            <td>ー</td>
                            <td>ー</td>
                        </tr>
                    </tbody>
                </table>
                <em>休診日：日曜日・祝祭日　△…土曜午前は9：00～13:00まで</em>
            </div>
            <!--/.tbl-->
            <div class="b_link_sp">
                <a href="#">
                    <picture>
                        <source media="(max-width:768px)" srcset="/common/images/img_ft_sp.png">
                        <img src="/common/images/img_ft_pc.png" alt="">
                    </picture>
                </a>
            </div>
            <!--/.b_link_sp-->
        </div>
        <!--/.b_flex-->
    </div>
</div>
<!--/.top_ft-->
<div class="bottom_ft">
    <p>Copyright &copy; i Dental Clinic. All Rights Reserved</p>
</div>
<!--/.bottom_ft-->