<div class="top_hd">
    <div class="row">
        <ul class="list_txt">
            <li>各種保険対応・予約制・急患随時受付可</li>
            <li>歯科・歯科口腔外科・小児歯科</li>
        </ul>
        <!--/.list_txt-->
        <p>大阪市西区九条の歯医者【韋歯科（い歯科）】です。<br class="show_sp">地下鉄中央線九条駅１Ｂ出口から徒歩1分</p>
    </div>
</div>
<!--/.top_hd-->
<div class="center_hd">
    <div class="row">
        <h1 class="logo">
            <a href="/"><img src="/common/images/logo.png" alt="韋歯科医院"></a>
        </h1>
        <!--/.logo-->
        <div class="btn_booking">
            <a href="#"><span>24時間簡単ネット予約</span></a>
        </div>
        <!--/.btn_booking-->
        <div class="b_tel">
            <em>ご予約・お問合せはこちらから</em>
            <p><ins>TEL</ins><a class="tel" href="tel:06-6584-4182">06-6584-4182</a></p>
        </div>
        <!--/.b_tel-->
    </div>
</div>
<!--/.center_hd-->
<div class="bottom_hd show_pc">
    <div class="row">
        <nav class="nav" id="nav">
            <ul>
                <li><a href="/"><img src="/common/images/mn_pc_01.png" alt="トップ"></a></li>
                <li><a href="/clinical_info/"><img src="/common/images/mn_pc_02.png" alt="診療案内"></a></li>
                <li><a href="/staff/"><img src="/common/images/mn_pc_03.png" alt="医院紹介"></a></li>
                <li><a href="/access/#recruit"><img src="/common/images/mn_pc_04.png" alt="スタッフ募集"></a></li>
                <li><a href="/access/"><img src="/common/images/mn_pc_05.png" alt="アクセス"></a></li>
            </ul>
        </nav>
        <!--/.nav-->
    </div>
</div>
<!--/.bottom_hd-->