<div class="b_nav show_sp">
    <ul class="nav_sp">
        <li><a href="/clinical_info/"><img src="/common/images/mn_sp_01.png" alt="診療案内"></a></li>
        <li><a href="/staff/"><img src="/common/images/mn_sp_02.png" alt="医院紹介"></a></li>
        <li><a href="/access/#recruit"><img src="/common/images/mn_sp_03.png" alt="スタッフ募集"></a></li>
        <li><a href="/access/"><img src="/common/images/mn_sp_04.png" alt="アクセス"></a></li>
    </ul>
    <!--/.nav_sp-->
</div>
<!--/.b_nav-->